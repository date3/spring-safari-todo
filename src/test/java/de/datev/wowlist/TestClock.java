package de.datev.wowlist;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.TemporalAmount;

public class TestClock extends Clock {

    private Instant currentTime;
    private final ZoneId zone;

    public TestClock(Instant currentTime, ZoneId zone) {
        this.currentTime = currentTime;
        this.zone = zone;
    }

    public static TestClock withUtc(Instant currentTime) {
        return new TestClock(currentTime, ZoneId.of("UTC"));
    }

    public void forward(TemporalAmount amount) {
        this.currentTime = this.currentTime.plus(amount);
    }

    @Override
    public ZoneId getZone() {
        return this.zone;
    }

    @Override
    public Clock withZone(ZoneId zone) {
        return null;
    }

    @Override
    public Instant instant() {
        return this.currentTime;
    }
}
