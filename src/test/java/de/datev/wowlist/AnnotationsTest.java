package de.datev.wowlist;

import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.util.Set;

import static org.reflections.scanners.Scanners.TypesAnnotated;

public class AnnotationsTest {

    /**
     * https://projects.haykranen.nl/java/
     */
    @Test
    public void annotationsScanningWithSpring() {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(CodeSmell.class));
        Set<BeanDefinition> definitions = scanner.findCandidateComponents("de.datev.wowlist");

        for (BeanDefinition d : definitions) {
            String className = d.getBeanClassName();
            String packageName = className.substring(0, className.lastIndexOf('.'));
            System.out.println("className:" + className);
        }
    }

    @Test
    public void annotationsScanningWithReflections() {
        Reflections reflections = new Reflections("de.datev.wowlist");

        Set<Class<?>> annotated =
                reflections.get(TypesAnnotated.with(CodeSmell.class).asClass());

        annotated.forEach(System.out::println);

        // --
        // logic here.


        // ---
    }
}
