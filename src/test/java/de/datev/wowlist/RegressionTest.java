package de.datev.wowlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.UUID;
@CodeSmell
public class RegressionTest {

    @Test
    public void koan(){
        String name1 = new String("m");
        String name2 = new String("m");

        Assertions.assertTrue(name1.equals(name2));
    }

    /**
     * This tests demonstrates why it is important to have a proper hashcode/equals implementation on
     * all entities.
     */
    @Test
    public void regression(){
        ArrayList<Todo> todos = new ArrayList<>();

        UUID uuid = UUID.randomUUID();

        Todo newTodo = new Todo("test");
        newTodo.setId(uuid);
        todos.add(newTodo);

        // time goes by... and a new object with the same properties is created
        // this could i.e. happen if a new HTTP call comes in
        Todo todoToDelete = new Todo("test");
        todoToDelete.setId(uuid);

        // if there is no hashcode/equals implementation, the remove() method can´t delete the todo
        // from collection because equals/hashcode from Object is used - which compares the object instance.
        Assertions.assertTrue(todos.remove(todoToDelete));
        Assertions.assertTrue(todos.isEmpty());
    }

}
