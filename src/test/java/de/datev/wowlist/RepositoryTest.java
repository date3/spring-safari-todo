package de.datev.wowlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@DataJpaTest
public class RepositoryTest {

    @Autowired
    TodoRepository todoRepository;


    // select t1_0.id,t1_0.description,t1_0.done,t1_0.due_date from todo t1_0 left join subtask s1_0 on t1_0.id=s1_0.todo_id where s1_0.description=?
    @Test
    public void test() {
        Todo todo = new Todo();
        todo.setDescription("a description");
        todo.setDueDate(Instant.now().plus(1, ChronoUnit.HOURS));

        Subtask subtask = new Subtask();
        subtask.setDescription("subtask description");
        subtask.setTodo(todo);

        todo.addSubtask(subtask);

        todoRepository.save(todo);

        List<Todo> result = todoRepository.findBySubtaskDescription("subtask description");

        Assertions.assertEquals(1, result.size());
    }
}
