package de.datev.wowlist;

public class SubTaskBuilder {

    String description;

    public Subtask build() {
        return new Subtask(description);
    }
}
