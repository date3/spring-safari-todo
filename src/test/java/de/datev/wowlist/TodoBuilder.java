package de.datev.wowlist;

import java.util.ArrayList;
import java.util.List;

/**
 * TodoBuilder to create complex test data.
 * <p>
 * References:
 * <p>
 * - http://www.growing-object-oriented-software.com/
 * - https://www.arhohuttunen.com/test-data-builders/
 */
public class TodoBuilder {

    String description = "default description";
    boolean done;

    List<SubTaskBuilder> subtasks = new ArrayList<>();

    public Todo build() {
        var todo = new Todo();
        todo.setDescription(description);
        todo.setDone(done);

        subtasks.forEach(s -> todo.addSubtask(s.build()));

        return todo;
    }

    TodoBuilder createTodoWithOneSubtask() {
        TodoBuilder builder = new TodoBuilder();
        Todo todo = builder
                .withoutDescription()
                .withSubtask(new SubTaskBuilder())
                .build();

        return builder;
    }

    public TodoBuilder withDescription(String description) {
        this.description = description;

        return this;
    }

    public TodoBuilder withDone(boolean done) {
        this.done = done;

        return this;
    }

    public TodoBuilder withoutDescription() {
        this.description = null;

        return this;
    }

    public TodoBuilder withSubtask(SubTaskBuilder builder) {
        this.subtasks.add(builder);

        return this;
    }
}
