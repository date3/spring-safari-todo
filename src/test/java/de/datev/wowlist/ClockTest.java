package de.datev.wowlist;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertTrue;

@CodeSmell
public class ClockTest {

    /**
     * Showcase how time testing with a clock can be achieved with the validation framework.
     */
    @Test
    public void validationWithClock() {
        // validator can be created with a clock that provides the clock
        Validator validator = Validation
                .byDefaultProvider()
                .configure()
                .clockProvider(() -> Clock.offset(Clock.systemDefaultZone(), Duration.ofSeconds(-100))) // travel back in time, "now" for validation framework is in the past now.
                .buildValidatorFactory().getValidator();

        Todo todo = new Todo();
        todo.setDescription("test");
        todo.setDueDate(Instant.now(Clock.systemDefaultZone()));

        var validate = validator.validate(todo);

        assertTrue(validate.isEmpty());
    }
}
