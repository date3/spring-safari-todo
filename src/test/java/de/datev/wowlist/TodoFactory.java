package de.datev.wowlist;

public class TodoFactory {

    // object mother pattern
    public static Todo createTodoWithSubtask(String subtaskDescription) {
        var todo = new Todo("kaffee kochen");
        var s = new Subtask(subtaskDescription);
        s.setTodo(todo);
        todo.addSubtask(s);

        return todo;
    }

    public static Todo createTodoWithoutSubtasks() {
        var todo = new Todo("kaffee kochen");
        var s = new Subtask("bohnen mahlen");
        s.setTodo(todo);
        todo.addSubtask(s);

        return todo;
    }

}
