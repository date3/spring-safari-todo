package de.datev.wowlist;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.time.Duration;

@SpringBootTest
@Import(TestConfiguration.class)
public class ClockIntegrationTest {

    @Autowired
    private TestClock testClock;

    @Test
    public void itCanForwardTheTestClock() {

        testClock.forward(Duration.ofHours(24));

    }
}
