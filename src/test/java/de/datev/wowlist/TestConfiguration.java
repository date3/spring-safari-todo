package de.datev.wowlist;

import org.springframework.context.annotation.Bean;

import java.time.Instant;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
    @Bean
    public TestClock clock() {
        return TestClock.withUtc(Instant.now());
    }
}
