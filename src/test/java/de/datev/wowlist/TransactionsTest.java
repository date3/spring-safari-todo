package de.datev.wowlist;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TransactionsTest {

    @Autowired
    TodoService todoService;

    /**
     * // transaction management programatically
     *
     * @Test
     * void givenAPayment_WhenUsingTxManager_ThenShouldCommit() {
     *     TransactionStatus status = transactionManager.getTransaction(definition);
     *     try {
     *         Payment payment = new Payment();
     *         payment.setReferenceNumber("Ref-1");
     *         payment.setState(Payment.State.SUCCESSFUL);
     *
     *         entityManager.persist(payment);
     *         transactionManager.commit(status);
     *     } catch (Exception ex) {
     *         transactionManager.rollback(status);
     *     }
     *     assertThat(entityManager.createQuery("select p from Payment p").getResultList()).hasSize(1);
     * }
     */

    /**
     * Implicit transaction management. This test will fail because transaction is closed at transaction boundary, here
     * the service class. Afterwards it´s not possible to access collection which are loaded lazily as default. There are
     * solutions to fix that.
     */
    @Test
    @Disabled
    public void implicitTransactionHandling() {
        var todo = new Todo("kaffee kochen");
        var s = new Subtask("bohnen mahlen");

        s.setTodo(todo);
        todo.addSubtask(s);

        todoService.createTodo(todo);

        List<Todo> allTodos = todoService.getAllTodos();
        allTodos.forEach(t -> t.getSubtask().forEach(System.out::println));
    }
}
