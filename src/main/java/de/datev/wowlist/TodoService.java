package de.datev.wowlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    @Autowired
    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    public Todo createTodo(Todo todoFromRequest) {
        return todoRepository.save(todoFromRequest);
    }

    public void changeTodo(UUID id, Todo changedTodo) {
        Todo todoToChange = getTodoById(id);
        todoToChange.setDescription(changedTodo.getDescription());

        todoRepository.save(todoToChange);
    }

    public void deleteTodo(UUID id) {
        todoRepository.deleteById(id);
    }

    public List<Todo> getTodoByDescription(String description) {
        return todoRepository.findByDescriptionContains(description);
    }

    public void createSubtask(UUID todoId, Subtask subtask) {
        Todo todoToChange = getTodoById(todoId);

        todoToChange.addSubtask(subtask);

        todoRepository.save(todoToChange);
    }

    private Todo getTodoById(UUID todoId) {
        return todoRepository.findById(todoId).orElseThrow(() -> new ResponseStatusException(HttpStatusCode.valueOf(404)));
    }

    public void createNote(UUID todoId, Note note) {
        Todo todoToChange = getTodoById(todoId);

        todoToChange.addNote(note);

        todoRepository.save(todoToChange);
    }
}
