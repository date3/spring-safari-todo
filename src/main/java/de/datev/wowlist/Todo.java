package de.datev.wowlist;

import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotBlank;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Todo {

    @Column
    @NotBlank
    private String description;

    @Column
    private boolean done;

    @Id
    private UUID id;

    @Future
    private Instant dueDate;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    private List<Subtask> subtask = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    private List<Note> notes = new ArrayList<>();

    // required by jpa
    Todo() {
        id = UUID.randomUUID();
    }

    public Todo(String description) {
        this.id = UUID.randomUUID();
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void addSubtask(Subtask subtask) {
        this.subtask.add(subtask);

        subtask.setTodo(this);
    }

    public void addNote(Note note) {
        this.notes.add(note);

        note.setTodo(this);
    }

    public List<Subtask> getSubtask() {
        return subtask;
    }

    public List<Note> getNotes() {
        return notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equals(id, todo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void setSubtask(List<Subtask> subtask) {
        this.subtask = subtask;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }
}
