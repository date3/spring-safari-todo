package de.datev.wowlist;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
public class Note {
    @Id
    UUID id;

    @Column
    String description;

    @ManyToOne
    @JoinColumn(name = "todo_id")
    Todo todo;

    public Note() {
        this.id = UUID.randomUUID();
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
