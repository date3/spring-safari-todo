package de.datev.wowlist;

import jakarta.persistence.*;

import java.util.Objects;
import java.util.UUID;

@Entity
public class Subtask {

    @Id
    UUID id;

    @Column
    String description;

    @ManyToOne
    @JoinColumn(name = "todo_id")
    Todo todo;

    public Subtask(String description) {
        this.id = UUID.randomUUID();
        this.description = description;
    }

    public Subtask() {
        this.id = UUID.randomUUID();
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subtask subtask = (Subtask) o;
        return Objects.equals(id, subtask.id) && Objects.equals(description, subtask.description) && Objects.equals(todo, subtask.todo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, todo);
    }
}
