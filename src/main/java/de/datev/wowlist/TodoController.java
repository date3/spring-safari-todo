package de.datev.wowlist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class TodoController {

    Logger logger = LoggerFactory.getLogger(TodoController.class);

    TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/todos")
    public List<Todo> getTodos(){
        return todoService.getAllTodos();
    }

    @GetMapping("/todos/query")
    public List<Todo> getTodosByDescription(@RequestParam("description") String description){
        return todoService.getTodoByDescription(description);
    }


    @PostMapping("/todos")
    @ResponseStatus(HttpStatus.CREATED)
    public Todo createTodo(@RequestBody CreateTodoRequest createTodoRequest){
        Todo todo = mapRequestToTodo(createTodoRequest);

        return todoService.createTodo(todo);
    }

    @PostMapping("/todos/{todoId}/subtasks")
    @ResponseStatus(HttpStatus.CREATED)
    public void createSubtask(@PathVariable UUID todoId, @RequestBody Subtask subtask){
        todoService.createSubtask(todoId, subtask);
    }
    @PostMapping("/todos/{todoId}/notes")
    @ResponseStatus(HttpStatus.CREATED)
    public void createNotes(@PathVariable UUID todoId, @RequestBody Note note){
        todoService.createNote(todoId, note);
    }

    @PutMapping("/todos/{id}")
    public void changeTodo(@PathVariable("id") UUID id, @RequestBody Todo changedTodo){
        todoService.changeTodo(id, changedTodo);
    }

    @DeleteMapping("/todos/{id}")
    public void deleteTodo(@PathVariable("id") UUID id){
        todoService.deleteTodo(id);
    }

    private Todo mapRequestToTodo(CreateTodoRequest createTodoRequest) {
        // map the dto to domain object
        return new Todo(createTodoRequest.getDescription());
    }
}
